package br.up.edu.oapp08;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        //Pegar as caixas de texto;
        EditText cxRaio = (EditText) findViewById(R.id.txtRaio);
        EditText cxAltura = (EditText) findViewById(R.id.txtAltura);
        EditText cxVolume = (EditText) findViewById(R.id.txtVolume);

        //Pegar os textos;
        String txtRaio = cxRaio.getText().toString();
        String txtAltura = cxAltura.getText().toString();

        //Converter os textos para números;
        double raio = Double.parseDouble(txtRaio);
        double altura = Double.parseDouble(txtAltura);

        //Calcular o volume;
        double volume = 3.14 * Math.pow(raio, 2) * altura;

        //NumberFormat nf = new DecimalFormat("##0.00");
        //cxVolume.setText(nf.format(volume));
        cxVolume.setText(String.format("%.2f", volume));
    }

}
